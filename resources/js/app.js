(function () {
    var app = angular.module('MultilingualApp', []);
    
    app.controller('LangController', function () {
        var langLocal = window.localStorage['lang'];
        
        this.setLang = function(lang){
            if(lang){
                this.lang = lang;
                window.localStorage['lang'] = lang;
            } 
            
            if(this.lang == 'PT'){
                this.lang = LangPT;    
            } else {
                this.lang = LangEN;
            }
        };
        this.setLang(langLocal);
    
    });
    
    var LangPT = {
        locale: 'pt',
        mainNav: {
            logo: "Marta Preuss",
            about: "Sobre",
            work: "Portfolio",
            resumee: "Curriculo",
            contact: "Contato"
        },
        welcome:{
            firstLine: "É front-ender desde 2006 e pode contribuir com código de qualidade para sua empresa.",
            knowMore: "Confira"
        },
        about:{
            title: "Comunicação web",
            subtitle: "Eu comunico com bits e bytes. Nem só de código é feita a web: ela é feita de gente. Meu trabalho é fazer seu sistema conversar com pessoas na vida real.",
            highlight1Title: "Wireframes e UX",
            highlights1Desc: "Qual a melhor forma para navegar na sua plataforma? Imagino como as informações vão ser dispostas no seu site.",
            highlight2Title: "Front-end",
            highlights2Desc: "Hora de colocar a mão na massa e transformar imagem em código limpo, identado e semântico.",
            highlight3Title: "Comunicação",
            highlights3Desc: "E tem mais: produção textual, edição de imagens e vídeos e o que mais for necessário para falar pela web.",
        },
        work:{
            title: "São quase 10 anos de experiência",
            subtitle: "Desde 2006 como front-ender, redatora de tecnologia, comunicadora digital."
        },
        resumee:{
            title: "E tem muito mais",
            subtitle: "Veja meu currículo completo para ter acesso a todas as informações",
            url: "resources/pdf/curriculo.pdf",
            resumeeTitle:"Currículo",
            resumeeDescription:"Veja meu currículo completo em PDF.",
            behance: "Veja meus trabalhos com design e UX no Behance",
            portfolio:"Veja todos os meus projetos com descrição."
        },
        contact:{
            title: "Obrigada",
            text:"Você pode me encontrar em <a href='http://marta.preuss.nom.br'>todas as redes sociais que eu uso</a> ou pelo e-mail <a href='mailto:marta.preuss@gmail.com'>marta.preuss@gmail.com</a>."
        }
    };
    var LangEN = {
        locale: 'en',
        mainNav: {
            logo: "Marta Preuss",
            about: "About",
            work: "Portfolio",
            resumee: "Resumée",
            contact: "Contact"
        },
        welcome:{
            firstLine: "Front-ender since 2006 and can help writing code with quality",
            knowMore: "See more"
        },
        about:{
            title: "Communication trough web",
            subtitle: "I communicate with bits and bytes. The web isn't made only of code, but of people. My job is to do your system talk to the real-world person.",
            highlight1Title: "UX and Wireframes",
            highlights1Desc: "Which way is the best to walk trough your site? I can figure out where the information will be placed.",
            highlight2Title: "Front-end",
            highlights2Desc: "Time to get my hands dirty doing clean, semantic HTML code and responsive CSS styles.",
            highlight3Title: "Communication",
            highlights3Desc: "And more: textual production, video and image edition and whatever is necessary to talk trough the web.",
        },
        work:{
            title: "Almost 10 years of experience",
            subtitle: "Since 2006 as front-ender, technology writer and digital communicator."
        },
        resumee:{
            title: "And there is more",
            subtitle: "Check out my resumée to know more",
            url: "resources/pdf/resumee.pdf",
            resumeeTitle:"Resumée",
            resumeeDescription:"Check out my resumée (pdf)",
            behance: "Check out my Behance portfolio",
            portfolio: "You can see my html jobs too (portuguese)"
        },
        contact:{
            title: "Thank you",
            text:""
        }
    };
    
})();