# HIRE ME #

This is a single page resume of my qualifications so you can hire me as front-end developer.

### How to install ###

* Clone repository
* Open index.html
* ???
* Profit!!!

Or just access http://tudo.marta.preuss.nom.br/hire-me

### Tecnologies used ###

* Semantic HTML 5
* Responsive CSS 3
* JS, jQuery and Backbone
* PSD: https://creativemarket.com/TheGravity/20548-Supernova-PSD-Template
* Ubuntu 10.15
* Brackets
* Less Autocompiler
* Emmet

### Contact ###

marta.preuss@gmail.com